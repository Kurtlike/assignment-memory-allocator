
//
// Created by kurtlike on 12/20/21.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
#include <stdio.h>
#define TEST_COUNT 6
typedef void (test)(FILE* output);

void test_successful_alloc(FILE* output);

void test_free_one_block(FILE* output);

void test_free_two_blocks(FILE* output);

void test_merging_blocks(FILE* output);

void test_extending(FILE* output);

void test_cant_extending(FILE* output);

static test* const tests[] = {
        test_successful_alloc,
        test_free_one_block,
        test_free_two_blocks,
        test_merging_blocks,
        test_extending,
        test_cant_extending
};
#endif //ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
