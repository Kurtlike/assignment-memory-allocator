
//
// Created by kurtlike on 12/20/21.
//

#include <stdio.h>
#include "test/test.h"
#include "main/util.h"

static FILE* open_file_to_write(const char* file_name) {
    return fopen(file_name, "w");
}

int main(void) {
    const char* file_name = "test.txt";
    FILE* file = open_file_to_write(file_name);
    for (size_t i = 0; i < TEST_COUNT; i++) {
        tests[i](file);
    }
    printf("Tests passed. \nResults in %s\n", file_name);
    return 0;
}
